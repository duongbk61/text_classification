#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import sys


from model.svm_model import SVMModel
from model.naive_bayes_model import NaiveBayesModel
# print(sys.path)

class TextClassificationPredict(object):
    def __init__(self):
        self.test = None

    def get_train_data(self):
        #  train data
        train_data = []
        f = open('train.txt', 'r', encoding='utf-8-sig')
        train_data = []
        for line in f:
            train_data.append({"feature": (line.split(':')[1].strip()).split(' ',2)[2], "target": line.split(':')[0]})
        df_train = pd.DataFrame(train_data)

        #  test data
        test_data = []
        f = open('test.txt', 'r', encoding='utf-8-sig')
        test_data = []
        for line in f:
            test_data.append({"feature": line.split(':', 1)[1], "target": line.split(':', 1)[0]})

        df_test = pd.DataFrame(test_data)

        # init model naive bayes
        model = SVMModel()

        clf = model.clf.fit(df_train["feature"], df_train.target)

        predicted = clf.predict(df_test["feature"])

        # Print predicted result
        print (predicted)
        print (clf.predict_proba(df_test["feature"]))

         # print (clf.check_proba))


if __name__ == '__main__':
    tcp = TextClassificationPredict()
    tcp.get_train_data()
